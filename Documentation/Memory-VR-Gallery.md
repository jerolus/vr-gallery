# VR Gallery
Two months ago i started an Udacity VR Nanodegree Course with the idea of increase my level and programmer skills.  
It based on make small projects to learn specific topics of Virtual Reality.

This is the second project of my degree and it took me like 3 weeks to complet it.  

Click on the thumbnail below to watch the full gameplay of my final result:

[![](Documentation/Screenshot1.png)](https://youtu.be/mWpWbZgAvD4)

# Process

## Idea
For this project Udacity gives you some tips to complete this task. I thought what to do in the project, and three big ideas come to me:    
1. The fist idea was to create an open space where you can go wherever you want and take information about the enviroment.
2. The second idea was to create a scene where you can teleport to other scenes and the enviroment is changing everytime.
3. The las idea was to create like am actual gallery to learn about an specific topic.

I choose the third one just because i thought that in the other ones i have to make a lot of art and that means i have to lost time making models, textures and animations.    
About this idea, the fist i thought was to left the user to move around the scene where the player wants.    
To implement this system Udacity provides you some example scenes
to make this possible.

## Scene sketching
I was not sure about how distribute my scene. I did some sketches to clarify my mind and start with a reference of what exactly i wanted to do.   
Finally i decided to take as reference the second one because it´s more intuitive and simple. That don´t mean that i took the easiest way,   
just i thought as a user and that gives you the best experience.

My sketch was something like this:

|                                     |                                     |
|  :------------------------------:   | :---------------------------------: |
|  ![01](Documentation/Sketch1.jpg)   |  ![02](Documentation/Sketch2.jpg)   |

## Building the scene
It took me a lot of time set all the parameters in the scene. I had to resume the texts and decide what to put in each wall.    
After some testers i decided to rescale all the assets and give them more realistic size.

After some builds the result was like this:

|                                         |                                         |
|  :----------------------------------:   | :-------------------------------------: |
|  ![07](Documentation/Screenshot7.PNG)   |  ![08](Documentation/Screenshot8.PNG)   |
|  ![09](Documentation/Screenshot9.PNG)   |  ![10](Documentation/Screenshot10.PNG)  |
|  ![11](Documentation/Screenshot11.PNG)  |  ![12](Documentation/Screenshot12.PNG)  |


## Test
This was probably the most important part of my project. The game was tested by some family and friends in different rounds of testing.      
In the first times testing, the big problem was the scale of the assets and the velocity of movement.   
After some hours of work fixing those problems a new round of testing was launched with better results.

Then i started to do my own script of RaycastInputBehaviour, which took me a lot of time fixing the bugs with the behaviour.    
I launched one more round of testing to know how much time you should look at the button to interact with it.   
This expertiment was so good and i pass to the next step.

In this case, I tryied to set correctly all the interactive videos with their respective sounds.
I launghed more rounds to know exactlyhow was the user experience.    
After fixing some bugs i launched the last round and it was so succesfull because more than 80% of users told me the experience in general was so good.

## Conclusion
In my opinion this project is the result of a hard work after leaving my daily job.   
That means that if i had more time to work on it, the project could be really cool giving it a good textures and improvement the user experience.   
I think this is the first of a lot of projects of Virtual Reality i´m going to do,    
because following this way i can understand better the mecanics that we use in Virtual Reality.   
Thanks to Google Udacity for this opportunity.      
I hope you liked it!

## Screenshots

|                                         |                                         |
|  :----------------------------------:   | :-------------------------------------: |
|  ![01](Documentation/Screenshot1.png)   |  ![02](Documentation/Screenshot2.png)   |
|  ![03](Documentation/Screenshot3.png)   |  ![04](Documentation/Screenshot4.png)   |
|  ![05](Documentation/Screenshot5.png)   |  ![06](Documentation/Screenshot6.png)   |

