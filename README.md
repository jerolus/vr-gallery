# VR Gallery

This is one of my projects for [Udacity Google VR Nanodegree Program](https://eu.udacity.com/course/vr-developer-nanodegree--nd017). <br>
With this game for VR you can learn about ancient cultures. You can move around the scene and interact with different videos.

|                                                    |                                                   |
|  :---------------------------------------------:   | :-----------------------------------------------: |
|  ![](Documentation/Screenshot1.png?raw=true "02")  | ![](Documentation/Screenshot6.png?raw=true "03")  |

## Setup

- [Unity 2018.1.1f1](https://unity3d.com/es/get-unity/download/archive)
- [GVR SDK for Unity v1.170.0](https://github.com/googlevr/gvr-unity-sdk)

## Contact

Name: Jesús María Parral Puebla <br>
Gmail: jesusmaria31@gmail.com <br>
Linkedin: [Profile](https://www.linkedin.com/in/jesusmariaparralpuebla/) <br>
YouTube: [Channel](https://www.youtube.com/channel/UC69skMwVNJglUjmYYsMLivw)

## Write-Up

[VR-Gallery Write-Up](https://gitlab.com/jerolus/vr-gallery/blob/master/Documentation/Memory-VR-Gallery.md)